const {User,validate}= require('../models/user.model');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');


exports.user_get_all = async (req, res,next) => {
    res.status(200).json({error:false , message : 'Welcome'});
};

exports.signup = (req, res,next) => {
    const { error } = validate(req.body);
    // validation
    if(error) return res.status(400).json({message :error.details[0].message, error : true});
    // check user already exits
    User.find({'user_email': req.body.email})
    .exec()
    .then(user => {
        if(user.length >= 1){
           return res.status(422).json({message : 'User Already exist !! Try to sign-in.',error:true});
        } else{
            bcrypt.hash(req.body.password,10,(err,hash)=>{
                if(err) return res.status(500).json({error : true,message:err });
                else{
                    const user = new User({
                        user_email : req.body.email,
                        user_password : hash
                    });
                    user
                    .save()
                    .then( result => {
                       return  res.status(201).json({message : 'User Created',error:false})
                        }
                    )
                    .catch(err => {
                       return  res.status(500).json({error :true , message: err });
                        }
                    );
                }
            });
        }
    })

};

exports.signin = (req, res,next) => {
    User.findOne({'user_email': req.body.email})
    .exec()
    .then(user => {
        console.log(user);
        if(user == undefined || user == null){
            return  res.status(401).json({error :true , message: 'Auth Failed' });
        }
        try{
            bcrypt.compare(req.body.password, user.user_password, function(err, result) {
                if(err) return  res.status(401).json({error :true , message: 'Auth Fails' });
                if(result) {
                    const token =   jwt.sign(
                        {
                            email : user.user_email,
                            userId : user._id,
                        },
                        process.env.JWT_KEY,
                        {
                            expiresIn : "2h"
                        }
                    );

                    console.log(process.env.JWT_KEY);
                    console.log(token);
                    return res.status(200).json({error :false , message: 'Log In',token:token });
                }
                else return res.status(401).json({error :true , message: 'Auth Fails' });
            });
        }
        catch(err) {
             return res.status(500).json({error :true , message: err });
        }
    })
    .catch(err => {
        return res.status(500).json({error :true , message: err });
    });
};
