const createError = require('http-errors');
const express = require('express');
const app = express();
// all packages
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');

require('dotenv').config();

//middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// app.use(expressValidator());

// db connection
const dbUrl = process.env.MONGODB_URL;
mongoose.connect(dbUrl,{useNewUrlParser: true,useUnifiedTopology:true})
.then(() => console.log('Connected To Mongo'))
.catch(err=>console.error('Not connected..',err));

// all routes
const userRouter = require('./routes/user');
app.use('/user', userRouter);

// app.use((req,res,next) => {
//     res.status(200).json({
//         msg : 'its working...!!'
//     });
// });
 // create 404
app.use(function(req, res, next) {
  next(createError(404));
});

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`));
module.exports = app;
