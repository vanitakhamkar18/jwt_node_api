const jwt =  require('jsonwebtoken');

module.exports = (req ,res, next) => {
    try {
        const token = req.headers.authorization;
        console.log('-----------------------');
        console.log(token);
        const decoded = jwt.verify(token, process.env.JWT_KEY);
        console.log(decoded);
        // req.userData = decoded;
        next()
    }
    catch(err){
       return res.status(401).json({error:true,message:"Auth Failed"});
    }
};

// module.exports = auth;
