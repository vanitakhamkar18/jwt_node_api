const mongoose = require('mongoose');
const Joi = require('joi');

const userSchema = mongoose.Schema({
    user_email : {
        type: String,
        required : true
    },
    user_password : {
        type: String,
        required : true
    },
    user_verified : {
        type : Number,
        default : 0
    },
    user_cdate : {
        type : Date,
        default:Date.now
    },
    user_udate : {
        type : Date,
        default : Date.now
    },
    active : {
        type : Number,
        default : 0
    },
    is_deleted : {
        type : Number,
        default : 0
    }
});

const User = mongoose.model('User', userSchema);


function validateUser(user){
    const schema = Joi.object({
        email : Joi.string().email().min(10).max(50).required(),
        password : Joi.string().regex(/^[a-zA-Z0-9]{6,16}$/).min(6).required()
    })
    return schema.validate(user);
}

exports.User = User;
exports.validate = validateUser;
