const express = require('express');
const userRouter = express.Router();
const checkAuth = require('../models/authCheck');

const UserController = require('../controllers/userController');

userRouter.get('/' ,UserController.user_get_all);

userRouter.post('/' , checkAuth , UserController.user_get_all);

userRouter.post('/sign-up', UserController.signup);

userRouter.post('/sign-in', UserController.signin);


module.exports = userRouter;
